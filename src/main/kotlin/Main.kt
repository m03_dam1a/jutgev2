import clases.Alumne
import clases.Problemes
import clases.Professor
import objects.Colors
import java.sql.Connection
import java.sql.DriverManager
import java.sql.SQLException
import java.time.LocalDate
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*
import kotlin.system.exitProcess


val scanner = Scanner(System.`in`).useLocale(Locale.UK)
val alumne = Alumne()
var connection: Connection? = null
fun main() {
    showBanner()
    login()

}



fun login() {
    val scanner = Scanner(System.`in`)
    var opcio = 34

    do {
        try {
            println(
                "${Colors.light_green}Login - ${Colors.blue} Tria el teu rol =>${Colors.reset} ■ 1 - Alumne ■ 2 - Professor ■ 0 - Sortir"
            )
            opcio = scanner.nextInt()

            when (opcio) {
                1 -> Alumne().menu()
                2 -> Professor().loginMestre()
                0 -> exitProcess(0)
                else -> println("Opcio no valida")
            }
        } catch (e: Exception) {
            println("S'ha produït un error: ${e.message}")
            scanner.nextLine()
        }
    } while (opcio != 0)
}



fun ajuda() {

    println(
        "Benvingut a la pagina d'ajuda:\n" +
                "Jutge es una eina de resolucio de problemes per a l'estudiant. \n" +
                "Per part del professor,l'app permet la gestio del progres de l'alumne\n" +
                "treient informes a mes de poder afegir mes problemes\n" +
                "es una aplicacio molt intuitiva."
    )

}

fun showBanner() {
    println(
        "${Colors.italic}${Colors.bold} Welcome to ${Colors.blue}" + "    ██ ██    ██ ██████   ██████  ███████     ██ ████████ ██████  \n" +
                "                ██ ██    ██ ██   ██ ██       ██          ██    ██    ██   ██ \n" +
                "                ██ ██    ██ ██   ██ ██   ███ █████       ██    ██    ██████  \n" +
                "           ██   ██ ██    ██ ██   ██ ██    ██ ██          ██    ██    ██   ██ \n" +
                "            █████   ██████  ██████   ██████  ███████     ██    ██    ██████  \n" +
                "                                                                   " +
                "                                                                    ${Colors.reset}"
    )


}




fun obtindreDataActual(): String {
    val fechaActual = LocalDate.now()
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
    return fechaActual.format(formatter)
}

fun obtindreHoraActualFormatejada(): String {
    val horaActual = LocalTime.now()
    val formatter = DateTimeFormatter.ofPattern("HH-mm-ss")

    return horaActual.format(formatter)
}

fun getId(problemes: Problemes): Int {

    return (problemes.problemes.size + 1)
}