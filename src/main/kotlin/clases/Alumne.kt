package clases

import objects.Colors
import ajuda
import alumne
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import login
import obtindreDataActual
import obtindreHoraActualFormatejada
import scanner
import java.io.File
import java.io.IOException
import java.util.*

class Alumne {

    val problemes = Problemes().problemes

    fun menu() {
        var opcio: Int
        do {
            try {
                println(
                    "${Colors.light_green}Menu -  ${Colors.reset}" +
                            "■ 1 - Seguir amb l'itinerari  " +
                            "■ 2 - Llista de problemes  " +
                            "■ 3 - Consultar historic de problemes resolts  " +
                            "■ 4 - Ajuda  " +
                            "${Colors.red}■ 0 - Sortir${Colors.reset}"
                )
                opcio = scanner.nextInt()

                when (opcio) {
                    1 -> alumne.seguirItinerari()
                    2 -> alumne.mostrarLlista()
                    3 -> alumne.consultarHistoric()
                    4 -> ajuda()
                    0 -> {
                        guardarReport()
                        login()
                    }

                    else -> println("Opcio no valida")
                }
            } catch (e: InputMismatchException) {
                println("Entrada invàlida. Si us plau, introdueix un número enter.")
                scanner.next()
                opcio = -1
            }
        } while (opcio != 0)

    }

    fun seguirItinerari() {

        val llistaProblemesAResoldre = mutableListOf<Problema>()
        for (problema in problemes) {
            if (!problema.resolt) {
                llistaProblemesAResoldre.add(problema)
            }
        }
        for (problema in llistaProblemesAResoldre) {
            problema.mostrarProblema()
            problema.resoldreProblema()
        }
        println("Sortint al menu principal...")
        menu()
    }


    fun mostrarLlista() {
        var sortir = false


        do {
            var n = 1
            for (problema in problemes) {
                println("${Colors.light_green}$n${Colors.reset} -  ${problema.enunciat}")
                n++
            }
            println("\nSi vols resoldre un dels problemes introdueix el seu numero \n" + " Si vols sortir introdueix s")
            var opcio = scanner.next()
            try {
                if (opcio == "s") return
                val opcioInt = opcio.toInt()
                if (opcioInt in 1..problemes.size) {
                    problemes[opcioInt - 1].mostrarProblema()
                    problemes[opcioInt - 1].resoldreProblema()
                } else {
                    println("opcio no valida. Torna a provar:")
                }
            } catch (e: NumberFormatException) {
                println("opcio no valida. Torna a provar:")
            }
        } while (!sortir)
        menu()

    }


    fun consultarHistoric() {
        println("${Colors.light_white_bg}${Colors.black}Historic de problemes resolts:${Colors.reset} ")
        for (problema in problemes) {
            if (problema.resolt) {
                problema.mostrarProblema()
                println(
                    "Numero d'intents: ${problema.nIntets}\n" +
                            "Intents:"
                )

                for (intents in problema.intents.indices) {
                    println("$intents - ${problema.intents[intents]}")
                }
            }
        }
    }

    fun guardarReport() {

        val arxiu = File("./src/data/registre-${obtindreDataActual()}-h-${obtindreHoraActualFormatejada()}.json")

        try {
            val json = Json.encodeToString(alumne.problemes)

            val escritor = arxiu.bufferedWriter()
            escritor.write(json)
            escritor.close()

            println("Problemes guardats en format JSON amb exit.")
        } catch (e: IOException) {
            println("Error al guardar los problemas en formato JSON: ${e.message}")
        }
    }


}
