package clases

import kotlinx.serialization.Serializable

@Serializable
class JocDeProves(val input: String, val output: String) {
}