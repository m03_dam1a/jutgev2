package clases

import connection
import java.sql.SQLException

class JutgeDB() {

    fun rebreProblemes(): MutableList<Problema> {
        val problemes = mutableListOf<Problema>()
        val query = connection!!.createStatement().executeQuery("SELECT * FROM Problema")

        try {
            while (query.next()) {
                val id_problema = query.getInt("id_problema")
                val titol = query.getString("titol")
                val enunciat = query.getString("enunciat")
                val entrada = query.getString("entrada")
                val sortida = query.getString("sortida")

                val jocDeProvesPublic = rebreJocDeProves(id_problema, "jocdeprovespublic_id")
                val jocDeProvesPrivat = rebreJocDeProves(id_problema, "jocdeprovesprivat_id")

                val problema = if (jocDeProvesPublic != null && jocDeProvesPrivat != null) {
                    Problema(
                        id_problema,
                        titol,
                        enunciat,
                        entrada,
                        sortida,
                        jocDeProvesPublic,
                        jocDeProvesPrivat
                    )
                } else if (jocDeProvesPublic != null) {
                    Problema(
                        id_problema,
                        titol,
                        enunciat,
                        entrada,
                        sortida,
                        jocDeProvesPublic
                    )
                } else if (jocDeProvesPrivat != null) {
                    Problema(
                        id_problema,
                        titol,
                        enunciat,
                        entrada,
                        sortida,
                        jocDeProvesPrivat
                    )
                } else {
                    Problema(
                        id_problema,
                        titol,
                        enunciat,
                        entrada,
                        sortida
                    )
                }

                problemes.add(problema)
            }
            query.close()
        } catch (e: SQLException) {
            println("Error ${e.errorCode} al rebre els problemes: ${e.message}")
        }

        return problemes
    }

    fun rebreJocDeProves(idProblema: Int, columna: String): JocDeProves? {
        val query = connection!!.prepareStatement("SELECT $columna FROM JocDeProves WHERE problema_id = ?")
        query.setInt(1, idProblema)
        val resultSet = query.executeQuery()

        return if (resultSet.next()) {
            val entrada = resultSet.getString("entrada")
            val sortida = resultSet.getString("sortida")
            JocDeProves(entrada, sortida)
        } else {
            null
        }
    }
}
