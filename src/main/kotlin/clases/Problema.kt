package clases

import objects.Colors
import kotlinx.serialization.Serializable
import scanner

@Serializable
data class Problema(
    val id_problema: Int,
    val titol: String,
    val enunciat: String,
    val entrada: String,
    val sortida: String,
    val jocDeProvesPublic: JocDeProves? = null,
    val jocDeProvesPrivat: JocDeProves? = null
) {
    var resolt = false
    var nIntets = 0
    var intents = mutableListOf<String>()


    fun mostrarProblema() {
        println(
            "${this.enunciat}\n" +
                    "${Colors.recuadro} ${this.jocDeProvesPublic?.input} ${Colors.reset} - " +
                    "${Colors.recuadro} ${this.jocDeProvesPublic?.output} ${Colors.reset}\n" +
                    "${Colors.recuadro} ${this.jocDeProvesPrivat?.input} ${Colors.reset} - " +
                    "${Colors.recuadro} [X] ${Colors.reset}\n  ${Colors.light_lila}Resposta: ${this.jocDeProvesPrivat?.output}${Colors.reset}"
        )
    }

    fun resoldreProblema() {
        var sortir = false

        do {
            println(
                "${Colors.blue}Vols resoldre'l?: " +
                        "${Colors.green} ■ 1 - si " +
                        "${Colors.yellow} ■ 2 - no " +
                        "${Colors.red} ■ 3 - sortir${Colors.reset}"
            )
            var resposta = scanner.next()
            try {
                val opcio = resposta.toInt()
                when (opcio) {
                    1 -> {
                        println("Inserta la teva resposta:")
                        var respostaUsuari = scanner.next()
                        if (respostaUsuari == this.jocDeProvesPrivat?.output) {
                            println("${Colors.light_cyan}✅ Correcte${Colors.reset}")
                            this.nIntets + 1
                            this.intents.add(respostaUsuari)
                            this.resolt = true
                            resolt = true
                            return

                        } else {
                            println("${Colors.red}❌ Error. Proba un altre cop.${Colors.reset}")
                            this.nIntets + 1
                            this.intents.add(respostaUsuari)
                        }
                    }

                    2 -> return
                    3 -> Alumne().menu()
                    else -> {
                        println("Introdueix ${Colors.light_cyan}1${Colors.reset} si vols o ${Colors.yellow}2${Colors.reset} si no en vols resoldre el problema o ${Colors.red}3${Colors.reset} si vols sortir.")
                    }
                }
            } catch (e: NumberFormatException) {
                println("Opció no vàlida. Introdueix un número.")
            }
        } while (!sortir)

    }


}


