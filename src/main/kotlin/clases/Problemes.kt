package clases

import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import kotlin.io.path.Path
import kotlin.io.path.readText

class Problemes {

    val problemes = mutableListOf<Problema>()


    init {
        try {
            val listaDeProblemasJson = Path("./src/data/problemes.json").readText()
            val listaDeProblemas = Json.decodeFromString<List<Problema>>(listaDeProblemasJson)
            for (problemaInfo in listaDeProblemas) {
                problemes.add(problemaInfo)
            }
        } catch (e: Exception) {
            println("Se produjo un error al cargar los problemas desde el archivo JSON: ${e.message}")
        }
    }

}